// Lệnh này tương tự import express from 'express' // Dùng để import thư viện express vào project
const express = require('express');
//Import router
const companiesRouter=require('./app/routes/companies');

// Khởi tạo app express
const app=express();
// Khai báo cổng project
const port=8000;

app.use((request,response,next)=>{
    console.log("Time ", new Date());
    next();
},
(request,response,next)=>{
    console.log("Request Method ", request.method);
    next();
}
);
//Cấu hình app đọc được body request JSON
app.use(express.json());
//Cấu hình app đọc được tieng Viet uft8
app.use(express.urlencoded({
    extended:true
}));
// Khai báo API dạng get '/'
//Call back function: Là một tham số của hàm khác và nó sẽ được thực thi sau khi hàm đó được call
app.get('/',(request,response) => {
    let today=new Date();
    response.status(200).json({
        message:`Xin chào hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()+1} năm ${today.getFullYear()}`
    });
})

app.use("/",companiesRouter);

//Chạy app express
app.listen(port,()=>{
    console.log(`App listening on port: ${port}`);
})
