//Import thu vien express
const { response } = require("express");
const express=require("express");
const { companiesObjectList } = require("../../module/companies"); // Tương tự: import {data} from "./data";
const router=express.Router();

router.get("/companies",(request,response)=>{
    let name=request.query.name;
    if(name){
        let ListRes=[];
        for(let i=0;i<companiesObjectList.length;i++){
            if(companiesObjectList[i].checkCompanyByCode(name)) {
                ListRes.push(companiesObjectList[i]);
            }
        };
        response.status(200).json({
            Company:ListRes
        });
    }else{
        response.status(200).json({
            Company:companiesObjectList
        });
    }
});
router.post("/companies",(request,response)=>{
    let Response = {id: 8,
                    name: "Alfreds",
                    contact: " Anders",
                    country: "Indian"
                    };

                    companiesObjectList.push(Response);

    response.status(200).json({
        Company: companiesObjectList
    })
});
router.get("/companies/:paramID",(request,response)=>{
    let paramID=request.params.paramID;
    // Do drinkId nhận được từ params là string nên cần chuyển thành kiểu integer
    paramID = parseInt(paramID); 

    // Khởi tạo biến drinkResponse là kết quả tìm được 
    let Response = null;

    // Duyệt từng phần tử của mảng để tìm được đồ uống có Id tương ứng
    for (let index = 0; index < companiesObjectList.length; index++) {
        if(companiesObjectList[index].id === paramID) {
            Response = companiesObjectList[index];
            // Nếu tìm được thì thoát khỏi vòng lặp ngay lập tức
            break;
        }
    };

    response.status(200).json({
        Company: Response
    })
});
router.put("/companies/:paramID",(request,response)=>{
    let paramID=request.params.paramID;
    // Do drinkId nhận được từ params là string nên cần chuyển thành kiểu integer
    paramID = parseInt(paramID); 

    // Khởi tạo biến drinkResponse là kết quả tìm được 
    let Response = {id: 8,
        name: "Alfreds",
        contact: " Anders",
        country: "Indian"
        };

    // Duyệt từng phần tử của mảng để tìm được đồ uống có Id tương ứng
    for (let index = 0; index < companiesObjectList.length; index++) {
        if(companiesObjectList[index].id === paramID) {
            companiesObjectList.splice(index, 1, Response)
            // Nếu tìm được thì thoát khỏi vòng lặp ngay lập tức
            break;
        }
    };

    response.status(200).json({
        Company: companiesObjectList
    })
});
router.delete("/companies/:paramID",(request,response)=>{
    let paramID=request.params.paramID;
    // Do drinkId nhận được từ params là string nên cần chuyển thành kiểu integer
    paramID = parseInt(paramID); 

    // Duyệt từng phần tử của mảng để tìm được đồ uống có Id tương ứng
    for (let index = 0; index < companiesObjectList.length; index++) {
        if(companiesObjectList[index].id === paramID) {
            companiesObjectList.splice(index, 1);
            // Nếu tìm được thì thoát khỏi vòng lặp ngay lập tức
            break;
        }
    };

    response.status(200).json({
        Company: companiesObjectList
    })
});


//Export data file => module
module.exports=router;