class Companies{
    constructor(paramId,paramName,paramContact,paramCountry){
        this.id=paramId;
        this.name=paramName;
        this.contact=paramContact;
        this.country=paramCountry;
    }
    
    checkCompanyById(paramid) {
        return this.id === paramid
    }
    checkCompanyByCode(paramName) {
        return this.name.includes(paramName.toUpperCase());
    }
}
let companiesObjectList = [
    {
        id: 1,
        name: "Alfreds Futterkiste",
        contact: "Maria Anders",
        country: "Germany"
    },
    {
        id: 2,
        name: "Centro comercial Moctezuma",
        contact: "Francisco Chang",
        country: "Mexico"
    },
    {
        id: 3,
        name: "Ernst Handel",
        contact: "Roland Mendel",
        country: "Austria"
    },
    {
        id: 4,
        name: "Island Trading",
        contact: "Helen Bennett",
        country: "UK"
    },
    {
        id: 5,
        name: "Laughing Bacchus Winecellars",
        contact: "Yoshi Tannamuri",
        country: "Canada"
    },
    {
        id: 6,
        name: "Magazzini Alimentari Riuniti",
        contact: "Giovanni Rovelli",
        country: "Italy"
    }
]

module.exports = {
    companiesObjectList
}